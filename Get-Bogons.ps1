function Get-Bogons {

    $results = (((((Invoke-WebRequest https://team-cymru.com/community-services/bogon-reference/bogon-dotted-decimal/).content -split "`n" | sls -Pattern "<b>[0-9]"| sls -Pattern " 2|224.0.0.0 15|240.0.0.0 15" -notmatch | Sort-Object | Get-Unique) -split "<b>") -split "<br /></b>") -split "</b><br />") | sls -Pattern "^$" -notmatch

    clear
    Write-Host "Cisco Standard ACL" -ForegroundColor Magenta
    foreach ($i in $results) {
	   Write-Host "access-list 10 deny $i log"
    }

    Write-Host "Cisco Extended ACL" -ForegroundColor Magenta
    foreach ($i in $results) {
        Write-Host "access-list 101 deny ip $i any log"
    }
}

Get-Bogons