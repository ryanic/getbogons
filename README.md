# Description

Simple PowerShell script to pull the latest Bogons from Team Cymru and display in Cisco standard and extended ACL formats.

![](https://gitlab.com/ryanic/getbogons/raw/master/demo_get-bogons.gif)